import {DateTime} from "luxon";

class User {
  constructor(email, password, age, userId = null, lastLogin = null) {
    if (userId) {
      this.userId = userId;
    }
    this.email = email;
    this.password = password;
    this.age = age;
    this.lastLogin = lastLogin;
  }

  toJSON() {
    return {
      id: this.userId,
      email: this.email,
      age: this.age || null,
      lastLogin: this.lastLogin ? this.lastLogin.toISO() : null,
    };
  }

  static fromDocument(doc) {
    return new User(doc.email, doc.password, doc.age, doc.userId, doc.lastLogin ? DateTime.fromISO(doc.lastLogin) : null);
  }
}

export default User;
