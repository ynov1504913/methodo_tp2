export const NO_SESSION_FOUND = "No session was found or the user does not exist";

export default {NO_SESSION_FOUND};
