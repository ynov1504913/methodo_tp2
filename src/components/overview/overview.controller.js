class OverviewController {
  constructor(overviewService) {
    this.overviewService = overviewService;
  }

  getUserStatistics = async (req, res) => {
    const { user_id } = req.params;
    return res.status(200).send(await this.overviewService.getUserStatistics(user_id));
  }

  getStatistics = async (req, res) => {
    return res.status(200).send(await this.overviewService.getStatistics());
  }
}
