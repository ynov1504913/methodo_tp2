// noinspection DuplicatedCode

import {describe, expect, it, jest} from '@jest/globals';
import overviewMessages from './overview.messages.js';
import OverviewService from './overview.service.js';
import {DateTime} from 'luxon';
import Session from '../session/session.entities.js';

describe("OverviewService", () => {


  const mockSessions = [
    new Session("0", DateTime.local(2024,1,1,7,0), DateTime.local(2024,1,1,22,0),0),
    new Session("0", DateTime.local(2024,1,2,9,0), DateTime.local(2024,1,2,13,0),15),
    new Session("0", DateTime.local(2024,1,2,15,0), DateTime.local(2024,1,2,23,30),0),
    new Session("0", DateTime.local(2024,1,3,6,0), DateTime.local(2024,1,3,19,0),40),
    new Session("0", DateTime.local(2024,1,4,9,0), DateTime.local(2024,1,4,22,30),20)
  ]

  describe("getUserStatistics", () => {
    it ("functional error - should throw an error if there is no session for the user", async () => {
      //GIVEN
      const mockOverviewRepository = {
        getAllSessionByUserId: jest.fn((id) => []),
        getAllSessions: jest.fn(() => [])
      }
      const customOverviewService = new OverviewService(mockOverviewRepository)
      //WHEN
      //THEN
      await expect(customOverviewService.getUserStatistics()).rejects.toThrow(overviewMessages.NO_SESSION_FOUND);
    })

    it ("nominal case - should return a structure with all the statistics of the user", async () => {
      //GIVEN
      const mockOverviewRepository = {
        getAllSessionByUserId: jest.fn((id) => mockSessions)
      }
      const customOverviewService = new OverviewService(mockOverviewRepository)
      //WHEN
      const res = await customOverviewService.getUserStatistics("0");
      //THEN
      expect(res.session_count).toEqual(5);
      expect(res.average).toEqual(10.8);
      expect(res.range_min).toEqual(4);
      expect(res.range_max).toEqual(15);
      expect(res.gap_min).toEqual(2);
      expect(res.gap_max).toEqual(14);
      expect(res.average_pause_per_session).toEqual(0.25);
      expect(res.pause_min).toEqual(15);
      expect(res.pause_max).toEqual(40);
    })
  })

  describe("getStatistics", () => {
    it ("functional error - should throw an error if there is no session found", async () => {
      //GIVEN
      const mockOverviewRepository = {
        getAllSessions: jest.fn((id) => [])

      }
      const customOverviewService = new OverviewService(mockOverviewRepository)
      //WHEN
      //THEN
      await expect(customOverviewService.getStatistics()).rejects.toThrow(overviewMessages.NO_SESSION_FOUND);
    })

    it ("nominal case - should return a structure with all the statistics of all the users", async () => {
      //GIVEN
      const mockOverviewRepository = {
        getAllSessions: jest.fn((id) => mockSessions)
      }
      const customOverviewService = new OverviewService(mockOverviewRepository)
      //WHEN
      const res = await customOverviewService.getStatistics();
      //THEN
      expect(res.session_count).toEqual(5);
      expect(res.average).toEqual(10.8);
      expect(res.range_min).toEqual(4);
      expect(res.range_max).toEqual(15);
      expect(res.gap_min).toEqual(2);
      expect(res.gap_max).toEqual(14);
      expect(res.average_pause_per_session).toEqual(0.25);
      expect(res.pause_min).toEqual(15);
      expect(res.pause_max).toEqual(40);
    })
  })
})

/**
 * nombre de session
 * moyenne par session
 * plage minimale
 * plage maximale
 * écart entre 2 min
 * écart entre 2 max
 * moyenne pause par session
 * pause max
 * pause min
 */
