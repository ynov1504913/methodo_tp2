import express from 'express';

class OverviewRouter {
  constructor(overviewController) {
    this.overviewController = overviewController;
  }

  getRouter() {
    const router = express.Router();
    router.route('/statistics').get(this.overviewController.getStatistics);
    router.route('/statistics/:user_id').get(this.overviewController.getUserStatistics);
    return router;
  }
}

export default OverviewRouter;
