import {DateTime} from 'luxon';
import overviewMessages from './overview.messages.js';

class OverviewService {
  constructor(overviewRepository) {
    this.overviewRepository = overviewRepository;
  }

  async getUserStatistics(user_id) {
    let sessions = await this.overviewRepository.getAllSessionByUserId(user_id);

    if (sessions.length === 0) {
      throw new Error(overviewMessages.NO_SESSION_FOUND)
    } else {
      return this.getOverviewValues(sessions);
    }
  }

  async getStatistics() {
    let sessions = await this.overviewRepository.getAllSessions();

    if (sessions.length === 0) {
      throw new Error(overviewMessages.NO_SESSION_FOUND)
    } else {
      return this.getOverviewValues(sessions);
    }
  }

  getOverviewValues(sessions) {
    return {
      session_count: sessions.length,
      average: this.getAverage(sessions),
      range_min: this.getRangeMin(sessions),
      range_max: this.getRangeMax(sessions),
      gap_min: this.getGapMin(sessions),
      gap_max: this.getGapMax(sessions),
      average_pause_per_session: this.getAveragePausePerSession(sessions),
      pause_min: this.getPauseMin(sessions),
      pause_max: this.getPauseMax(sessions),
    };
  }

  getAverage(sessions) {
    let sum = 0;
    sessions.forEach(session => {
      sum = sum + session.endDate.diff(session.startDate);
    })
    return (sum/sessions.length)/3600000;
  }

  getRangeMin(sessions) {
    let calc = 0;
    let min = 0;
    sessions.forEach(session => {
       calc = session.endDate.diff(session.startDate);
       if (calc < min || min === 0) {
         min = calc;
       }
    });
    return (min/3600000);
  }

  getRangeMax(sessions) {
    let calc = 0;
    let max = 0;
    sessions.forEach(session => {
      calc = session.endDate.diff(session.startDate);
      if (calc > max) {
        max = calc;
      }
    });
    return (max/3600000);
  }

  getGapMin(sessions) {
    let min = 0;
    let calc = 0;
    let session1, session2;
    for (let i=0; i < sessions.length-1; i++) {
      session1 = sessions[i];
      session2 = sessions[i+1];
      calc = session2.startDate.diff(session1.endDate);
      if (calc < min || min === 0) {
        min = calc;
      }
    }
    return (min/3600000);
  }

  getGapMax(sessions) {
    let max = 0;
    let calc = 0;
    let session1, session2;
    for (let i=0; i < sessions.length-1; i++) {
      session1 = sessions[i];
      session2 = sessions[i+1];
      calc = session2.startDate.diff(session1.endDate);
      if (calc > max ) {
        max = calc;
      }
    }
    return (max/3600000);
  }

  getAveragePausePerSession(sessions) {
    let sum = 0;
    sessions.forEach(session => {
      sum = sum + session.pause;
    })
    return (sum/sessions.length)/60;
  }

  getPauseMin(sessions) {
    let min = 0;
    sessions.forEach(session => {
      if ((session.pause < min || min === 0) && (session.pause !== 0)) {
        min = session.pause;
      }
    });
    return (min);
  }

  getPauseMax(sessions) {
    let max = 0;
    sessions.forEach(session => {
      if (session.pause > max) {
        max = session.pause;
      }
    });
    return (max);
  }
}

export default OverviewService;
