import OverviewController from './overview.controller.js';
import OverviewService from './overview.service.js';
import OverviewRouter from './overview.router.js';
import OverviewRepository from './overview.repository.js';
import db from '../../db.js';

const overviewRepository = new OverviewRepository(db);
const overviewService = new OverviewService(overviewRepository);
const overviewController = new OverviewController(overviewService);
const overviewRouter = new OverviewRouter(overviewController);

export default {
    service: overviewService,
    controller: overviewController,
    router: overviewRouter.getRouter(),
};
