import Session from '../session/session.entities.js';
import User from '../user/user.entities.js';

class OverviewRepository {
  constructor(db) {
    this.db = db;
    this.collection = this.db.collection('sessions')
  }

  async getAllSessionByUserId(userId) {
    const query = {userId: userId};
    const documents = await this.collection.find(query).toArray();
    return documents.map(doc => Session.fromDocument(doc));
  }

  async getAllSessions() {
    const documents = await this.collection.find({}).toArray();
    return documents.map(doc => Session.fromDocument(doc));
  }
}

export default OverviewRepository;
