import Session from './session.entities.js';
import { ObjectId } from 'mongodb';
import { Duration } from 'luxon';
import TimeRemaining from './session.presentation.timeRemaining.js';

class SessionRepository {
    constructor(db) {
        this.db = db;
        this.collection = this.db.collection('sessions');
    }

    /**
     * Récupère une session par son id
     * @param sessionId {string}
     * @returns {Promise<Session>}
     */
    async getById(sessionId) {
        return Session.fromDocument(await this.collection.findOne({_id: ObjectId(sessionId)}));
    }

    /**
     * Récupère la dernière session de l'utilisateur
     * @param userId {string}
     * @returns {Promise<Session>}
     */
    async getLastSessionByUserId(userId) {
        const query = {userId: userId};
        const sort = {startDate: -1};
        return Session.fromDocument(await this.collection.findOne(query).sort(sort));
    }

    /**
     * Ajoute une date de fin à une session par son identifiant
     * @param sessionId {string}
     * @param endDate {DateTime}
     * @returns {Promise<*>}
     */
    async addEndDate(sessionId, endDate) {
        const query = {_id: ObjectId(sessionId)};
        const update = {$set: {endDate: endDate}};
        return this.collection.updateOne(query, update)
    }

    /**
     * Ajoute une session
     * @param session {Session}
     * @returns l'id de la session ajoutée {Promise<string[]>}
     */
    async addSession(session) {
        return this.collection.insertOne(session.toJSON()).getUpsertedIds();
    }

    /**
     * Met à jour la pause d'une session
     * @param sessionId
     * @param pause
     * @returns {Promise<void>}
     */
    async updatePause(sessionId, pause) {
        const query = {_id: ObjectId(sessionId)};
        const update = {$set: {pause: pause}};
        return this.collection.updateOne(query, update);
    }

    /**
     * Récupère la dernière session de l'utilisateur dont la date de début est la plus proche de la date donnée
     * @param userId
     * @param startDate
     * @returns {Promise<Session>}
     */
    async getLastSessionWithClosestStartDateByUserId(userId, startDate) {
        const query = {userId: userId, startDate: {$lte: startDate}};
        const sort = {startDate: -1};
        return Session.fromDocument(await this.collection.findOne(query).sort(sort));
    }

    /**
     * Récupère la prochaine session de l'utilisateur dont la date de fin est la plus proche de la date donnée
     * @param userId
     * @param endDate
     * @returns {Promise<Session>}
     */
    async getNextSessionWithClosestEndDateByUserId(userId, endDate) {
        const query = {userId: userId, endDate: {$gte: endDate}};
        const sort = {endDate: 1};
        return Session.fromDocument(await this.collection.findOne(query).sort(sort));
    }

    /**
     * Récupère la prochaine session de l'utilisateur dont la date de début est la plus proche de la date donnée
     * @param userId
     * @param startDate
     * @returns {Promise<Session>}
     */
    async getNextSessionWithClosestStartDateByUserId(userId, startDate) {
        const query = {userId: userId, startDate: {$gte: startDate}};
        const sort = {startDate: 1};
        return Session.fromDocument(await this.collection.findOne(query).sort(sort));
    }

    /**
     * Récupère toutes les sessions dont startDate.days est égal à day
     * @param day
     * @returns {Promise<Session[]>}
     */
    async getSessionsByDay(day) {
        const query = { startDate: { $dayOfMonth: day } };
        return this.collection.find(query).toArray();
    }

    /**
     * /!\ Fonction privé du service que l'on met dans le repo pour pouvoir la mocker /!\
     * Récupère le temps de test restant pour completer la journée
     * @param session {Session}
     * @returns {Promise<TimeRemaining>}
     */
    async getTimeRemainingForSessionDay(session) {
        const MINIMUM_TIME_TEST_FOR_A_DAY = Duration.fromObject({ hours: 15 });
        const sessions = await this.getSessionsByDay(session.endDate);

        const globalTestTimeForDay = sessions
          // On enlève toutes les sessions dont on ne peut pas déterminer la durée
          .filter(session => session.endDate)
          // On additionne le temps de chaque session et on soustrait le temps de pause
          .reduce((acc, session) => {
              acc = acc.plus(session.endDate.diff(session.startDate));
              if (session.pause) {
                  acc = acc.minus(session.pause);
              }
              return acc;
          }, Duration.fromObject({ minutes: 0 }));
        const timeRemaining = MINIMUM_TIME_TEST_FOR_A_DAY.minus(globalTestTimeForDay);

        return new TimeRemaining(session,
          timeRemaining < 0
            ? Duration.fromObject({ seconds: 0 })
            : timeRemaining.toISOTime({ suppressMilliseconds: true}));
    }
}

export default SessionRepository;
