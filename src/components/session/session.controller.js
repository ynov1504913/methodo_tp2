import Session from './session.entities.js';

class SessionController {
    constructor(sessionService) {
        this.sessionService = sessionService;
    }

    /**
     *  Commencer ou terminer une session
     * - Si l'utilisateur n'a pas de session en cours, la session commence
     * - Si l'utilisateur a une session en cours, la session se termine
     */
    checkSession = async (req, res) => {
        const { user_id } = req.params;
        return res.status(201).send(await this.sessionService.checkSession(user_id));
    }

    /**
     * Ajout d'une session complète pour un utilisateur
     */
    addSession = async (req, res) => {
        const { user_id } = req.params;
        const session = new Session(user_id ,req.body.startDate, req.body.endDate, req.body.pause);
        return res.status(201).send(await this.sessionService.addSession(session))
    }

    /**
     * Lors de l'ajout de la pause, si la session est en cours, la pause est ajoutée à la session en cours.
     *  - Si session_id null, on récupère la dernière session de l'utilisateur et on ajoute la pause à cette session
     *  - Sinon, on ajoute la pause à la session
     */
    pauseSession = async (req, res) => {
        const { user_id } = req.params;
        const sessionId = req.body.session_id;
        const pause = req.body.pause;

        return res.status(201)
            .send(await this.sessionService.pauseSession(user_id, sessionId, pause));
    }
}

export default SessionController;
