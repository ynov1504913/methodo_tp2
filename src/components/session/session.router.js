import express from 'express';

class SessionRouter {
    constructor(sessionController) {
        this.sessionController = sessionController;
    }

    getRouter() {
        const router = express.Router();
        router.route('/check/:user_id').post(this.sessionController.checkSession);
        router.route('/:user_id').post(this.sessionController.addSession);
        router.route('/pause/:user_id').put(this.sessionController.pauseSession);
        return router;
    }
}

export default SessionRouter;
