const SESSION_CONFLICT = 'Session added is in conflict with an existing session';
const SESSION_CONFLICTS = 'Session added is in conflict with several existing sessions';
const SESSION_NESTED = 'One or more sessions are nested in the session added';
const NEGATIVE_PAUSE_VALUE = 'Pause cannot be negative';
const PAUSE_TOO_LONG = "Pause cannot be greater than the session's duration";
const WRONG_DATES = 'Start date cannot be greater than end date';
const INVALID_DATE_FORMAT = 'Invalid date format';
const SESSION_NOT_FOUND = 'Session does not exist';

export default {
  SESSION_CONFLICT,
  SESSION_CONFLICTS,
  SESSION_NESTED,
  NEGATIVE_PAUSE_VALUE,
  PAUSE_TOO_LONG,
  WRONG_DATES,
  INVALID_DATE_FORMAT,
  SESSION_NOT_FOUND
};
