import { DateTime, Duration } from 'luxon';
import {USER_NOT_FOUND} from "../user/user.messages.js"
import sessionMessages from './session.messages.js';
import Session from './session.entities.js';
import TimeRemaining from './session.presentation.timeRemaining.js';

class SessionService {
    constructor(sessionRepository, userService) {
        this.sessionRepository = sessionRepository;
        this.userService = userService;
    }

    async checkSession(user_id) {
        let user = await this.userService.getUserById(user_id);
        if (user == null) {
            throw new Error(USER_NOT_FOUND);
        }
        let session = await this.sessionRepository.getLastSessionByUserId(user_id);
        const now = DateTime.now();
        if (session != null) {
            await this.sessionRepository.addEndDate(session.sessionId, now);
            session.endDate = now;
        } else {
            session =  new Session(user_id, now, null, 0);
            session.sessionId = await this.sessionRepository.addSession(session)[0];
        }

        return await this.sessionRepository.getTimeRemainingForSessionDay(session);
    }

    async addSession(session) {
        if (!DateTime.fromISO(session.startDate).isValid
          || !DateTime.fromISO(session.endDate).isValid) {
            throw new Error(sessionMessages.INVALID_DATE_FORMAT);
        }

        if (session.startDate > session.endDate) {
            throw new Error(sessionMessages.WRONG_DATES);
        }

        if (session.pause < 0) {
            throw new Error(sessionMessages.NEGATIVE_PAUSE_VALUE);
        }

        if (session.startDate.plus(session.pause) > session.endDate) {
            throw new Error(sessionMessages.PAUSE_TOO_LONG);
        }

        const user = await this.userService.getUserById(session.userId);
        if (user == null) {
            throw new Error(USER_NOT_FOUND);
        }

        const lastSessionWithClosestStart =
            this.sessionRepository.getLastSessionWithClosestStartDateByUserId(session.userId, session.startDate);

        // Une partie de la session à ajouter entre en conflit avec une session précédente
        if (lastSessionWithClosestStart != null && lastSessionWithClosestStart.endDate > session.startDate) {
            throw new Error(sessionMessages.SESSION_CONFLICT);
        }

        const nextSessionWithClosestEndDate =
            this.sessionRepository.getNextSessionWithClosestEndDateByUserId(session.userId, session.endDate);

        // Une partie de la session à ajouter entre en conflit avec une session suivante
        if (nextSessionWithClosestEndDate != null && nextSessionWithClosestEndDate.startDate < session.endDate) {
            throw new Error(sessionMessages.SESSION_CONFLICT);
        }

        const nextSessionWithClosestStartDate =
            this.sessionRepository.getNextSessionWithClosestStartDateByUserId(session.userId, session.startDate);

        // Une ou plusieurs sessions existantes sont incluses dans la session à ajouter
        if (nextSessionWithClosestStartDate != null && nextSessionWithClosestStartDate.endDate < session.endDate) {
            throw new Error(sessionMessages.SESSION_NESTED);
        }

        session.sessionId = this.sessionRepository.addSession(session)[0];

        return await this.sessionRepository.getTimeRemainingForSessionDay(session);
    }

    async pauseSession(user_id, sessionId, pause) {
        const user = await this.userService.getUserById(user_id)
        if (user == null) {
            throw new Error(USER_NOT_FOUND);
        }
        let session;

        if (sessionId == null) {
            session = await this.sessionRepository.getLastSessionByUserId(user_id);
        } else {
            session = await this.sessionRepository.getById(sessionId);
        }

        if (session === undefined || session.userId !== user_id) {
            throw new Error(sessionMessages.SESSION_NOT_FOUND);
        }

        const pauseDuration = Duration.fromObject({ minutes: pause });
        session.pause = pauseDuration.plus(session.pause ?? Duration.fromObject({ minutes: 0 }));

        if (session.endDate && session.startDate.plus(session.pause) > session.endDate) {
            throw new Error(sessionMessages.PAUSE_TOO_LONG);
        }

        await this.sessionRepository.updatePause(session.sessionId, pause);

        return await this.sessionRepository.getTimeRemainingForSessionDay(session);
    }

}

export default SessionService;
