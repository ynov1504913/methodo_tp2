import SessionController from './session.controller.js';
import SessionService from './session.service.js';
import SessionRouter from './session.router.js';
import SessionRepository from './session.repository.js';
import db from '../../db.js';
import service from '../user/user.module.js';

const sessionRepository = new SessionRepository(db);
const sessionService = new SessionService(sessionRepository, service);
const sessionController = new SessionController(sessionService);
const sessionRouter = new SessionRouter(sessionController);



export default {
    service: sessionService,
    controller: sessionController,
    router: sessionRouter.getRouter(),
};
