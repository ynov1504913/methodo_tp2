import crypto from 'crypto';
import { DateTime } from 'luxon';

class Session {
    constructor(userId, startDate, endDate, pause, sessionId = null) {
        this.sessionId = sessionId ?? crypto.randomUUID();
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.pause = pause;
    }

    toJSON() {
        return {
            sessionId: this.sessionId,
            userId: this.userId,
            startDate: this.startDate,
            endDate: this.endDate,
            pause: this.pause,
        };
    }

    static fromDocument(doc) {
        return new Session(doc.sessionId, doc.userId, doc.startDate ? DateTime.fromISO(doc.startDate) : null, doc.endDate ? DateTime.fromISO(doc.endDate) : null, doc.pause ? DateTime.fromISO(doc.pause) : null);
    }
}

export default Session;
