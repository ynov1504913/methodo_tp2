import { beforeEach, describe, expect, it, jest } from '@jest/globals';
import { DateTime, Duration } from 'luxon';

import SessionService from './session.service.js';

import User from '../user/user.entities.js';
import Session from './session.entities.js';

import userMessages from '../user/user.messages.js';
import sessionMessages from './session.messages.js';
import TimeRemaining from './session.presentation.timeRemaining.js';

describe('SessionService', () => {

  const USER_ID = '0';
  const USER_ID_KO = '42';
  const SESSION_ID = '0';
  const PAUSE = 15;
  const TIME_REMAINING_FOR_NO_SESSION = Duration.fromObject({ hours: 15 }).toISOTime({ suppressMilliseconds: true});

  /**
   * 2024-01-01T00:00:00
   * @type {*|DateTime}
   */
  const DEFAULT_START_DATE = DateTime.local(2024);

  /**
   * 2024-01-01T10:00:00
   * @type {*|DateTime}
   */
  const DEFAULT_END_DATE = DateTime.local(2024, 1, 1, 10);

  const USER_OK = new User('john.doe@gmail.com', 'azerty', 32, USER_ID);

  // 10-hour long session with 1 second pause
  let DEFAULT_SESSION = new Session(
    USER_ID,
    DEFAULT_START_DATE,
    DEFAULT_END_DATE,
    Duration.fromObject({ seconds: 1 }),
    SESSION_ID
  );

  // 1-hour long session with 0 pause
  const SECOND_SESSION = new Session(
    USER_ID,
    DEFAULT_END_DATE.plus({ seconds: 1 }),
    DEFAULT_END_DATE.plus({ hours: 1 }),
    0,
    SESSION_ID + 1
  );

  // next day 1-hour long session with 0 pause
  const THIRD_SESSION = new Session(
    USER_ID,
    DEFAULT_END_DATE.plus({ days: 1 }),
    DEFAULT_END_DATE.plus({
      days: 1,
      hours: 1
    }),
    Duration.fromObject({ seconds: 1 }),
    SESSION_ID + 2
  );

  // Util function dateToStringWithouMilliseconds
  const dateToStringWithoutMilliseconds = (date) => date.toISO().split('.')[0];

  const mockUserService = {
    getUserById: jest.fn((id) => USER_OK),
  };

  const mockSessionRepository = {
    getById: jest.fn(() => DEFAULT_SESSION),
    getLastSessionByUserId: jest.fn((userId) => DEFAULT_SESSION),
    addSession: jest.fn((session) => SESSION_ID),
    getLastSessionWithClosestStartDateByUserId: jest.fn((userId, startDate) => DEFAULT_SESSION),
    getNextSessionWithClosestEndDateByUserId: jest.fn((userId, startDate) => SECOND_SESSION),
    getNextSessionWithClosestStartDateByUserId: jest.fn((userId, startDate) => SECOND_SESSION),
    getTimeRemainingForSessionDay: jest.fn(() => new TimeRemaining(DEFAULT_SESSION, TIME_REMAINING_FOR_NO_SESSION))
  };

  beforeEach(() => {
    DEFAULT_SESSION = new Session(
      USER_ID,
      DEFAULT_START_DATE,
      DEFAULT_END_DATE,
      Duration.fromObject({ seconds: 1 }),
      SESSION_ID
    );
  });

  const sessionService = new SessionService(mockSessionRepository, mockUserService);

  describe('checkSession', () => {
    it('functional error - should throw an error if the user is not found', async () => {
      //GIVEN
      const customMockUserService = {
        getUserById: jest.fn(() => undefined)
      };
      const customSessionService = new SessionService(mockSessionRepository, customMockUserService);
      //WHEN
      //THEN
      await expect(customSessionService.checkSession(USER_ID))
        .rejects
        .toThrow(userMessages.USER_NOT_FOUND);
    });

    it('nominal case - when the last session is not finished, finish it and return it', async () => {
      //GIVEN
      const customSession = new Session(USER_ID, DEFAULT_START_DATE, undefined, undefined);
      const customSessionSecond = new Session(USER_ID, DEFAULT_START_DATE, DEFAULT_END_DATE, undefined, customSession.sessionId);
      const customMockSessionRepository = {
        getLastSessionByUserId: jest.fn((userId) => customSession),
        addEndDate: jest.fn((sessionId, endDate) => customSessionSecond),
        getTimeRemainingForSessionDay: jest.fn(() => new TimeRemaining(customSession, TIME_REMAINING_FOR_NO_SESSION))
      };
      const customSessionService = new SessionService(customMockSessionRepository, mockUserService);

      // On supprime les millisecondes pour éviter les erreurs de comparaison
      const now1 = dateToStringWithoutMilliseconds(DateTime.now());

      //WHEN
      const res = await customSessionService.checkSession(USER_ID);

      //THEN
      expect(res.session.sessionId)
        .toBe(customSession.sessionId);
      expect(dateToStringWithoutMilliseconds(res.session.endDate))
        .toBe(now1);
      expect(res.timeRemaining)
        .toBe(TIME_REMAINING_FOR_NO_SESSION);
    });

    it('nominal case - when the last session ended, create a new session and return it', async () => {
      //GIVEN
      const newStartDate = DateTime.now();
      const customSessionSecond = new Session(USER_ID, newStartDate, null, 0,  SESSION_ID);
      const customMockSessionRepository = {
        getLastSessionByUserId: jest.fn((userId) => null),
        addSession: jest.fn((session) => SESSION_ID),
        getTimeRemainingForSessionDay: jest.fn(() => new TimeRemaining(customSessionSecond, TIME_REMAINING_FOR_NO_SESSION))
      };

      const customSessionService = new SessionService(customMockSessionRepository, mockUserService);
      //WHEN
      const res = await customSessionService.checkSession(USER_ID);

      //THEN
      expect(res.session.sessionId).toBe(customSessionSecond.sessionId);
      expect(res.session.userId).toBe(customSessionSecond.userId);
      expect(dateToStringWithoutMilliseconds(res.session.startDate))
        .toBe(dateToStringWithoutMilliseconds(customSessionSecond.startDate));
      expect(res.session.endDate).toBe(customSessionSecond.endDate);
      expect(res.session.pause).toBe(customSessionSecond.pause);
      expect(res.timeRemaining)
        .toBe(TIME_REMAINING_FOR_NO_SESSION);
    });
  });

  describe('addSession', () => {
    it('functional error - should throw an error if one of the incoming dates have invalid format', async () => {
      // GIVEN
      DEFAULT_SESSION.startDate = "toto";

      // WHEN / THEN
      await expect(sessionService.addSession(DEFAULT_SESSION))
        .rejects
        .toThrow(sessionMessages.INVALID_DATE_FORMAT);
    });

    it('functional error - should throw an error if the incoming startDate is greater than the incoming endDate', async () => {
      // GIVEN
      const sessionToAdd = new Session(USER_ID, DEFAULT_END_DATE, DEFAULT_START_DATE, Duration.fromObject({ seconds: 1 }));

      // WHEN / THEN
      await expect(sessionService.addSession(sessionToAdd))
        .rejects
        .toThrow(sessionMessages.WRONG_DATES);
    });

    it('functional error - should throw an error if the incoming pause negative', async () => {
      // GIVEN
      const sessionToAdd = new Session(USER_ID, DEFAULT_START_DATE, DEFAULT_END_DATE, Duration.fromObject({ seconds: -1 }));

      // WHEN / THEN
      await expect(sessionService.addSession(sessionToAdd))
        .rejects
        .toThrow(sessionMessages.NEGATIVE_PAUSE_VALUE);
    });

    it('functional error - should throw an error if the incoming pause is greater than the session\'s duration', async () => {
      // GIVEN
      const sessionToAdd = new Session(USER_ID, DEFAULT_START_DATE, DEFAULT_END_DATE,
        DEFAULT_END_DATE.diff(DEFAULT_START_DATE)
          .plus({ seconds: 1 }));

      // WHEN / THEN
      await expect(sessionService.addSession(sessionToAdd))
        .rejects
        .toThrow(sessionMessages.PAUSE_TOO_LONG);
    });

    it('functional error - should throw an error if the user is not found', async () => {
      //GIVEN
      const customMockUserService = {
        getUserById: jest.fn(() => undefined)
      };
      const customSessionService = new SessionService(mockSessionRepository, customMockUserService);
      //WHEN
      //THEN
      await expect(customSessionService.addSession(DEFAULT_SESSION))
        .rejects
        .toThrow(userMessages.USER_NOT_FOUND);
    });

    it('functional error - should throw an error if the incoming start-date is included in an existing session', async () => {
      // GIVEN
      const sessionToAdd = new Session(USER_ID, DEFAULT_START_DATE.plus({ hours: 1 }), DEFAULT_END_DATE, 0);

      //WHEN
      //THEN
      await expect(sessionService.addSession(sessionToAdd))
        .rejects
        .toThrow(sessionMessages.SESSION_CONFLICT);
    });

    it('functional error - should throw an error if the incoming end-date is included in an existing session', async () => {
      // GIVEN
      const sessionToAdd = new Session(USER_ID, DEFAULT_END_DATE, DEFAULT_END_DATE.plus({ seconds: 2 }), 0);
      const SECOND_SESSION = new Session(
        USER_ID,
        DEFAULT_END_DATE.plus({ seconds: 1 }),
        DEFAULT_END_DATE.plus({ hours: 1 }),
        0,
        '0'
      );
      // WHEN / THEN
      await expect(sessionService.addSession(sessionToAdd))
        .rejects
        .toThrow(sessionMessages.SESSION_CONFLICT);
    });

    it('functional error - should throw an error if One or more sessions are nested in the incoming session', async () => {
      // GIVEN
      const sessionToAdd = new Session(
        USER_ID,
        SECOND_SESSION.startDate.minus({ seconds: 1 }),
        SECOND_SESSION.endDate.plus({ seconds: 1 }),
        0
      );

      const sessionBefore = new Session(
        USER_ID,
        SECOND_SESSION.startDate.minus({ hours: 1 }),
        SECOND_SESSION.startDate.minus({ minutes: 1 }),
        0
      );

      const sessionAfter = new Session(
        USER_ID,
        SECOND_SESSION.endDate.plus({ minutes: 1 }),
        SECOND_SESSION.endDate.plus({ hours: 1 }),
        0
      );

      const customMockSessionRepository = {
        getLastSessionWithClosestStartDateByUserId: jest.fn((userId, startDate) => sessionBefore),
        getNextSessionWithClosestEndDateByUserId: jest.fn((userId, startDate) => sessionAfter),
        getNextSessionWithClosestStartDateByUserId: jest.fn((userId, startDate) => SECOND_SESSION),
      };
      const customSessionService = new SessionService(customMockSessionRepository, mockUserService);

      // WHEN / THEN
      await expect(customSessionService.addSession(sessionToAdd))
        .rejects
        .toThrow(sessionMessages.SESSION_NESTED);
    });

    it('nominal case - when all fields are ok and there is no conflict with other sessions', async () => {
      //GIVEN
      const sessionAfter = null;

      const customMockSessionRepository = {
        getLastSessionWithClosestStartDateByUserId: jest.fn((userId, startDate) => SECOND_SESSION),
        getNextSessionWithClosestEndDateByUserId: jest.fn((userId, startDate) => sessionAfter),
        getNextSessionWithClosestStartDateByUserId: jest.fn((userId, startDate) => sessionAfter),
        addSession: jest.fn((session) => SESSION_ID),
        getTimeRemainingForSessionDay: jest.fn(() => new TimeRemaining(THIRD_SESSION, TIME_REMAINING_FOR_NO_SESSION))
      };
      const customSessionService = new SessionService(customMockSessionRepository, mockUserService);

      // WHEN
      const res = await customSessionService.addSession(THIRD_SESSION);

      //THEN
      expect(res.session)
        .toBe(THIRD_SESSION);
      expect(res.timeRemaining)
        .toBe(TIME_REMAINING_FOR_NO_SESSION);
    });
  });

  describe('pauseSession', () => {
    it('nominal case - without sessionId - should add the pause to the session', async () => {
      // GIVEN
      const customSession = DEFAULT_SESSION;
      customSession.pause = DEFAULT_SESSION.pause.plus(Duration.fromObject({ minutes: PAUSE }));

      const customMockSessionRepository = {
        getLastSessionByUserId: jest.fn(() => DEFAULT_SESSION),
        updatePause: jest.fn((sessionId, pause) => customSession),
        getTimeRemainingForSessionDay: jest.fn(() => new TimeRemaining(customSession, TIME_REMAINING_FOR_NO_SESSION))
      };

      const customSessionService = new SessionService(customMockSessionRepository, mockUserService);

      // WHEN
      const res = await customSessionService.pauseSession(USER_ID, null, PAUSE);

      // THEN
      expect(res.session)
        .toBe(customSession);
      expect(res.timeRemaining)
        .toBe(TIME_REMAINING_FOR_NO_SESSION);
    });

    it('nominal case - with sessionId - should add the pause to the session', async () => {
      // GIVEN
      const customSession = DEFAULT_SESSION;
      customSession.pause = DEFAULT_SESSION.pause.plus(Duration.fromObject({ minutes: PAUSE }));


      const customMockSessionRepository = {
        getById: jest.fn((USER_ID) => DEFAULT_SESSION),
        updatePause: jest.fn((sessionId, pause) => customSession),
        getTimeRemainingForSessionDay: jest.fn(() => new TimeRemaining(customSession, TIME_REMAINING_FOR_NO_SESSION))
      };

      const customSessionService = new SessionService(customMockSessionRepository, mockUserService);

      // WHEN
      const res = await customSessionService.pauseSession(USER_ID, DEFAULT_SESSION.sessionId, PAUSE);

      // THEN
      expect(res.session)
        .toBe(customSession);
      expect(res.timeRemaining)
        .toBe(TIME_REMAINING_FOR_NO_SESSION);
    });

    it('error case - userId doesn\'t exist - should throw an exception', async () => {
      // GIVEN
      const customMockUserService = {
        getUserById: jest.fn((USER_ID) => undefined)
      };

      const customSessionService = new SessionService(mockSessionRepository, customMockUserService);

      // WHEN / THEN
      await expect(customSessionService.pauseSession(USER_ID, PAUSE))
        .rejects
        .toThrow(userMessages.USER_NOT_FOUND);
    });

    it('error case - sessionId doesn\'t exist - should throw an exception', async () => {
      // GIVEN

      const customMockSessionRepository = {
        getLastSessionByUserId: jest.fn((USER_ID) => DEFAULT_SESSION),
        getById: jest.fn((sessionId) => undefined)
      };

      const customSessionService = new SessionService(customMockSessionRepository, mockUserService);

      // WHEN / THEN
      await expect(customSessionService.pauseSession(USER_ID, SESSION_ID, PAUSE))
        .rejects
        .toThrow(sessionMessages.SESSION_NOT_FOUND);
    });

    it('error case - sessionId isn\'t linked to user - should throw an exception', async () => {
      // GIVEN
      const customSession = new Session(USER_ID_KO, DEFAULT_START_DATE, null, null);

      const customMockUserService = {
        getUserById: jest.fn((USER_ID) => USER_OK)
      };

      const customMockSessionRepository = {
        getById: jest.fn((sessionId) => customSession)
      };

      const customSessionService = new SessionService(customMockSessionRepository, customMockUserService);

      // WHEN / THEN
      await expect(customSessionService.pauseSession(USER_ID, SESSION_ID, PAUSE))
        .rejects
        .toThrow(sessionMessages.SESSION_NOT_FOUND);
    });

    it("error case - with new pause, session's pause is longer than session itself - should throw an exception", async () => {
      // GIVEN
      // WHEN / THEN
      await expect(sessionService.pauseSession(USER_ID, SESSION_ID, DEFAULT_END_DATE.diff(DEFAULT_START_DATE, 'minutes').minutes + 1))
        .rejects
        .toThrow(sessionMessages.PAUSE_TOO_LONG);
    });
  });
});
