class TimeRemaining {
  constructor(session, timeRemaining) {
    this.session = session;
    this.timeRemaining = timeRemaining;
  }
}

export default TimeRemaining;
