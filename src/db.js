import { MongoClient, ServerApiVersion } from 'mongodb';

const uri = 'mongodb+srv://billaudmael:1234@methodo-test.zjsrfww.mongodb.net/?retryWrites=true&w=majority&appName=Methodo-Test';

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

let conn;
try {
  conn = await client.connect();
} catch (e) {
  console.error(e);
}
const db = conn.db('users');
export default db;
