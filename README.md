# Building a Flexible and Scalable Node.js Backend with Express: A Step-by-Step Tutorial
This tutorial walks through the project structure, implementing the controller, service, router and using the dependency injection pattern in a nodejs express application.

# Installation
You can follow this tutorial and understand how this project work on this medium [link](https://medium.com/@csalazar94/building-a-flexible-and-scalable-node-js-backend-with-express-a-step-by-step-tutorial-5a8633335b48).

```
npm install
```

# Usage
```
npm start 
```

# Logique Métier

## /session/check/:user_id
- On récupère la dernière session de l'utilisateur
  - Si session en cours, on ajoute un endDate à la session
  - Si session terminée, on crée une nouvelle session avec startDate uniquement

## /session/:user_id

- hasSessionConflict(startDate, endDate): sessionId | null
  - Vérifie si la session a un conflit avec une session existante
  - Si conflit, retourne l'id de la session en conflit
  - Sinon, retourne null

## /session/pause/:user_id

- Si session_id null, on récupère la dernière session de l'utilisateur et on ajoute la pause à cette session
- Sinon, on ajoute la pause à la session

Lors de l'ajout de la pause, si la session est en cours, la pause est ajoutée à la session en cours.


# API Endpoints

## User

### POST /users
Create a new user.

#### Request Body
```
{
  "name": "John Doe",
  "email": "johndoe@example.com",
  "password": "password123"
}
```

#### Response Body
```
{
  "id": 1,
  "name": "John Doe",
  "email": "johndoe@example.com"
}
```

### GET /users
Get a list of all users.

#### Response Body
```
[
  {
    "id": 1,
    "name": "John Doe",
    "email": "johndoe@example.com"
  },
  {
    "id": 2,
    "name": "Jane Smith",
    "email": "janesmith@example.com"
  }
]
```

### GET /users/:id
Get one user by id.

#### Response Body
```
{
    "id": 1,
    "name": "John Doe",
    "email": "johndoe@example.com"
}
```
## Session

Une session est une période de temps pendant laquelle un utilisateur porte des lentilles

### POST /session/check
Commencer ou terminer une session
- Si l'utilisateur n'a pas de session en cours, la session commence
- Si l'utilisateur a une session en cours, la session se termine

#### Request Body
```
{
  "userId": 1
}
```

#### Response Body
```
{
  "sessionId": 1,
  "userId": 1,
  "startDate": "2024-01-01T00:00:00.000Z",
  "endDate": "2024-01-01T02:00:00.000Z", // Null si la session est en cours
  "pause": 0
}
```

### POST /session
Ajout d'une session complète pour un utilisateur

#### Request Body
```
{
  "userId": 1,
  "startDate": "2024-01-01T00:00:00.000Z",
  "endDate": "2024-01-01T02:00:00.000Z",
  "pause": 0 // Optionnel
}
```

#### Response Body
```
{
  "sessionId": 1,
  "userId": 1,
  "startDate": "2024-01-01T00:00:00.000Z",
  "endDate": "2024-01-01T02:00:00.000Z", // Null si la session est en cours
  "pause": 0
}
```

### PUT /session/pause
Ajoute une pause dans une session existante

#### Request Params
```
{
  "userId": 1
  "session_id": 1, // Optionnel, si null, ajoute dans la dernière session
  "pause": 15 // Durée de la pause en minutes
}

```

#### Response Body
```
{
  "sessionId": 1,
  "userId": 1,
  "startDate": "2024-01-01T00:00:00.000Z",
  "endDate": "2024-01-01T02:00:00.000Z",
  "pause": 15
}
```
